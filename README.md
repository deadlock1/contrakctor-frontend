### Running the application

###### Using the pipeline artifact

- Download the deployment artifact.
- Install serve using `npm i -g serve.` Maybe you will need root tho install it.
- Extract the build folder inside the artifact.zip and inside the artifact folder run `serve build`.
- Application will run on port 5000.

###### Downloading the repository

- Inside the root of the project, run an npm install command
- Run the `build` command
- You can both use `serve build` command at the dist folder or `npm start` if you want to debug the application.

##### NOTES

- You must have contraktor_backend app running for this application work properly.
