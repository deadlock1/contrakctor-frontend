class ContractDriver {
  constructor() {}

  public async getContracts(title: string): Promise<ContractResponse> {
    return {
      expiration: "01/01/01",
      file: "doc.pdf",
      id: "123",
      startDate: "01/01/01",
      title: title,
    };
  }

  public async registerContract(contract: Contract): Promise<void> {
    Promise.resolve(contract);
  }

  public async uploadFile(file: File, contractId: string): Promise<void> {
    Promise.resolve({ file, contractId });
  }

  public async updateContract(associate: AssociatePart): Promise<void> {
    Promise.resolve(associate);
  }

  public async deleteContract(id: string): Promise<void> {
    Promise.resolve(id);
  }
}

export default new ContractDriver();
