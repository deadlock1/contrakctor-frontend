class PartDriver {
  constructor() {}

  public async getParts(document: string): Promise<PartResponse> {
    return Promise.resolve({
      document: document,
      email: "email@email.com",
      id: "1234",
      name: "nameasync",
      phone: {
        ddd: "12",
        number: "123456789",
      },
      secondName: "second",
    });
  }

  public async registerPart(part: Part): Promise<void> {
    Promise.resolve(part);
  }

  public async updatePart(id: string, part: Part): Promise<void> {
    Promise.resolve({ id: id, part: part });
  }

  public async deletePart(id: string): Promise<void> {
    Promise.resolve(id);
  }
}

export default new PartDriver();
