import { cleanup, fireEvent, render, screen } from "@testing-library/react";
import { BrowserRouter, Route } from "react-router-dom";
import { Associate } from "./index";

jest.mock("../../../api/contracts");

afterEach(cleanup);

describe("Register contracts screen", () => {
  test("matching snapshot", () => {
    render(
      <BrowserRouter>
        <Route>
          <Associate />
        </Route>
      </BrowserRouter>
    );
    expect(screen).toMatchSnapshot();
  });

  test("two-way binding input values", () => {
    render(
      <BrowserRouter>
        <Route>
          <Associate />
        </Route>
      </BrowserRouter>
    );
    const input = screen.getByPlaceholderText(
      "Título do contrato"
    ) as HTMLInputElement;

    fireEvent.change(input, { target: { value: "Contrato" } });

    expect(input.value).toBe("Contrato");
  });

  test("button should be disable if any input is empty", () => {
    render(
      <BrowserRouter>
        <Route>
          <Associate />
        </Route>
      </BrowserRouter>
    );
    const button = screen.getByText("Associar") as HTMLButtonElement;

    expect(button.disabled).toBeTruthy();
  });

  test("button should be enabled all inputs are filled", () => {
    render(
      <BrowserRouter>
        <Route>
          <Associate />
        </Route>
      </BrowserRouter>
    );
    const titleInput = screen.getByPlaceholderText(
      "Título do contrato"
    ) as HTMLInputElement;

    fireEvent.change(titleInput, { target: { value: "Contrato" } });

    expect(titleInput.value).toBe("Contrato");

    const documentInput = screen.getByPlaceholderText(
      "CPF da parte"
    ) as HTMLInputElement;

    fireEvent.change(documentInput, { target: { value: "05307401922" } });

    const button = screen.getByText("Associar") as HTMLButtonElement;
    expect(button.disabled).toBeFalsy();
  });
});
