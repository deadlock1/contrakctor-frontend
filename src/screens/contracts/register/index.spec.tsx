import { fireEvent, render, screen } from "@testing-library/react";
import user from "@testing-library/user-event";
import { BrowserRouter, Route } from "react-router-dom";
import { Contracts } from "./index";

jest.mock("../../../api/contracts");

beforeEach(() => {
  render(
    <BrowserRouter>
      <Route>
        <Contracts />
      </Route>
    </BrowserRouter>
  );
});

describe("Register contracts screen", () => {
  test("matching snapshot", () => {
    expect(screen).toMatchSnapshot();
  });

  test("two-way binding input values", () => {
    const input = screen.getByPlaceholderText(
      "Título do contrato"
    ) as HTMLInputElement;

    fireEvent.change(input, { target: { value: "Contrato" } });

    const dataInput = screen.getByPlaceholderText(
      "Data de início do contrato"
    ) as HTMLInputElement;

    fireEvent.change(dataInput, { target: { value: "01/01/01" } });

    expect(dataInput.value).toBe("01/01/01");
    expect(input.value).toBe("Contrato");
  });

  test("button should be disable if any input is empty", () => {
    const button = screen.getByText("Cadastrar") as HTMLButtonElement;

    expect(button.disabled).toBeTruthy();
  });

  test.skip("button should be enabled all inputs are filled", () => {
    const titleInput = screen.getByPlaceholderText(
      "Título do contrato"
    ) as HTMLInputElement;

    fireEvent.change(titleInput, { target: { value: "Contrato" } });

    const startInput = screen.getByPlaceholderText(
      "Data de início do contrato"
    ) as HTMLInputElement;

    fireEvent.change(startInput, { target: { value: "01/01/01" } });

    const expirationInput = screen.getByPlaceholderText(
      "Data de expiração do contrato"
    ) as HTMLInputElement;

    fireEvent.change(expirationInput, { target: { value: "01/01/01" } });

    const file = new File(["(⌐□_□)"], "chucknorris.pdf", {
      type: "application/pdf",
    });

    const fileInput = screen.getByPlaceholderText(
      "Anexar pdf do contrato"
    ) as HTMLInputElement;

    user.upload(fileInput, file);

    //file are not in the input - why?

    const button = screen.getByText("Cadastrar") as HTMLButtonElement;
    expect(button.disabled).toBeFalsy();
  });
});
