import React, { ChangeEvent, useEffect, useState } from "react";

import driver from "../../../api/contracts";

import { MainContainer } from "../../mainContainer";
import { Input, Button } from "../../../components";

import "./contracts.scss";

export const Contracts: React.FC = () => {
  const initialState = {
    title: "",
    expiration: "",
    startDate: "",
    file: "",
  };
  const [form, setForm] = useState<Contract>(initialState);
  const [file, setFile] = useState<File>();
  const [buttonDisabled, setButtonDisabled] = useState(true);

  const handleChange = async (
    key: keyof Contract,
    event: ChangeEvent<HTMLInputElement>
  ) => {
    let value = event.currentTarget.value;

    if (event.currentTarget.files) {
      setFile(event.currentTarget.files[0]);
    }

    setForm((prev) => ({
      ...prev,
      [key]: value,
    }));
  };

  const handleSave = async () => {
    try {
      await driver.registerContract(form);
      if (file) {
        await driver.uploadFile(file, "fakeId");
      }
      setForm(initialState);
    } catch (error) {
      console.log("error", { ...error });
    }
  };

  useEffect(() => {
    setButtonDisabled(() => {
      const values = Object.values(form);
      for (const value of values) {
        if (!value) {
          return true;
        }
      }
      return false;
    });
  }, [form]);

  return (
    <MainContainer>
      <div className="contract-container">
        <h1 className="contract-title">Contrato</h1>
        <div className="contract-form">
          <Input
            id="title-input"
            label="Título"
            placeholder="Título do contrato"
            onChange={(event) => handleChange("title", event)}
            value={form.title}
          />
          <Input
            label="Data de início"
            placeholder="Data de início do contrato"
            onChange={(event) => handleChange("startDate", event)}
            value={form.startDate}
          />
          <Input
            label="Data de expiração"
            placeholder="Data de expiração do contrato"
            onChange={(event) => handleChange("expiration", event)}
            value={form.expiration}
          />
          <Input
            type="file"
            label="Arquivo"
            placeholder="Anexar pdf do contrato"
            onChange={(event) => handleChange("file", event)}
            value={form.file}
          />
          <div className="button-container">
            <Button
              buttonTitle="Cadastrar"
              id="contract-save-button"
              onClick={handleSave}
              disabled={buttonDisabled}
            />
          </div>
        </div>
      </div>
    </MainContainer>
  );
};
