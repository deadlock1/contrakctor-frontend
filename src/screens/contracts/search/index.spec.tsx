import {
  cleanup,
  fireEvent,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import { BrowserRouter, Route } from "react-router-dom";
import { ContractSearch } from "./index";
import userEvent from "@testing-library/user-event";

jest.mock("../../../api/contracts");

afterEach(cleanup);

describe("Register contracts search screen", () => {
  test("matching snapshot", () => {
    render(
      <BrowserRouter>
        <Route>
          <ContractSearch />
        </Route>
      </BrowserRouter>
    );
    expect(screen).toMatchSnapshot();
  });

  test("two-way binding input values", () => {
    render(
      <BrowserRouter>
        <Route>
          <ContractSearch />
        </Route>
      </BrowserRouter>
    );
    const input = screen.getByPlaceholderText(
      "Título para pesquisa"
    ) as HTMLInputElement;

    fireEvent.change(input, { target: { value: "Contrato" } });

    expect(input.value).toBe("Contrato");
  });

  test("button should be disable if any input is empty", () => {
    render(
      <BrowserRouter>
        <Route>
          <ContractSearch />
        </Route>
      </BrowserRouter>
    );
    const button = screen.getByText("Pesquisar") as HTMLButtonElement;

    expect(button.disabled).toBeTruthy();
  });

  test("button should be enabled if all inputs are filled", () => {
    render(
      <BrowserRouter>
        <Route>
          <ContractSearch />
        </Route>
      </BrowserRouter>
    );
    const input = screen.getByPlaceholderText(
      "Título para pesquisa"
    ) as HTMLInputElement;

    fireEvent.change(input, { target: { value: "Contrato" } });
    const button = screen.getByText("Pesquisar") as HTMLButtonElement;

    expect(button.disabled).toBeFalsy();
  });

  test("should update values when data is fetched", async () => {
    const screen = render(
      <BrowserRouter>
        <Route>
          <ContractSearch />
        </Route>
      </BrowserRouter>
    );

    const inputTitle = screen.getByPlaceholderText("Título para pesquisa");
    expect(inputTitle).toBeDefined();

    userEvent.type(inputTitle, "Contrato");
    expect(inputTitle).toHaveValue("Contrato");

    const button = screen.getByText("Pesquisar") as HTMLButtonElement;
    expect(button.disabled).toBeFalsy();

    await waitFor(() => {
      userEvent.click(button);
    });

    const inputStartDate = screen.getByPlaceholderText(
      "Data de início do contrato"
    );
    expect(inputStartDate).toHaveValue("01/01/01");
  });
});
