import { render } from "@testing-library/react";
import { Parts } from "./index";
import { BrowserRouter, Route } from "react-router-dom";
import userEvent from "@testing-library/user-event";

describe("Part Register", () => {
  test("render", () => {
    const { container } = render(
      <BrowserRouter>
        <Route>
          <Parts />
        </Route>
      </BrowserRouter>
    );

    expect(container).toBeDefined();
  });

  test("button should be disabled", () => {
    const { getByText } = render(
      <BrowserRouter>
        <Route>
          <Parts />
        </Route>
      </BrowserRouter>
    );

    const button = getByText("Cadastrar") as HTMLButtonElement;

    expect(button.disabled).toBeTruthy();
  });

  test("two way bind input values", () => {
    const { getByPlaceholderText } = render(
      <BrowserRouter>
        <Route>
          <Parts />
        </Route>
      </BrowserRouter>
    );

    const input = getByPlaceholderText("Nome da parte") as HTMLInputElement;
    userEvent.type(input, "Diogo");
    expect(input).toHaveValue("Diogo");
  });

  test("update values", () => {
    const { getByPlaceholderText } = render(
      <BrowserRouter>
        <Route>
          <Parts />
        </Route>
      </BrowserRouter>
    );

    const input = getByPlaceholderText("Nome da parte") as HTMLInputElement;
    userEvent.type(input, "Diogo");
    expect(input).toHaveValue("Diogo");
  });
});
