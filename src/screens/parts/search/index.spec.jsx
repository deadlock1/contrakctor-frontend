import { fireEvent, render, waitFor } from "@testing-library/react";
import { PartSearch } from "./index";
import { BrowserRouter, Route } from "react-router-dom";
import userEvent from "@testing-library/user-event";

jest.mock("../../../api/parts");

describe("Part Register", () => {
  test("render", () => {
    const { container } = render(
      <BrowserRouter>
        <Route>
          <PartSearch />
        </Route>
      </BrowserRouter>
    );

    expect(container).toBeDefined();
  });

  test("button should be disable if any input is empty", () => {
    const { getByText } = render(
      <BrowserRouter>
        <Route>
          <PartSearch />
        </Route>
      </BrowserRouter>
    );

    const button = getByText("Pesquisar");

    expect(button.disabled).toBeTruthy();
  });

  test("two way bind input values", () => {
    const { getByPlaceholderText } = render(
      <BrowserRouter>
        <Route>
          <PartSearch />
        </Route>
      </BrowserRouter>
    );

    const input = getByPlaceholderText("CPF da Parte");
    userEvent.type(input, "Diogo");
    expect(input).toHaveValue("Diogo");
  });

  test("update values when data is fetched", async () => {
    const screen = render(
      <BrowserRouter>
        <Route>
          <PartSearch />
        </Route>
      </BrowserRouter>
    );
    const documentInput = screen.getByPlaceholderText("CPF da Parte");

    const button = screen.getByText("Pesquisar");

    const inputName = screen.getByPlaceholderText("Nome da parte");

    fireEvent.change(documentInput, { target: { value: "05307401922" } });
    expect(documentInput).toHaveValue("05307401922");

    await waitFor(() => {
      userEvent.click(button);
      expect(inputName).toHaveValue("nameasync");
    });
  });
});
